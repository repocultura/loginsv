## Librería para la autenticación de LoginSV para Laravel.
[![Latest Stable Version](http://poser.pugx.org/Reposmc/loginsv/v)](https://packagist.org/packages/Reposmc/loginsv) 
[![Total Downloads](http://poser.pugx.org/Reposmc/loginsv/downloads)](https://packagist.org/packages/Reposmc/loginsv) 
[![Latest Unstable Version](http://poser.pugx.org/Reposmc/loginsv/v/unstable)](https://packagist.org/packages/Reposmc/loginsv) 
[![License](http://poser.pugx.org/Reposmc/loginsv/license)](https://packagist.org/packages/Reposmc/loginsv) 
[![PHP Version Require](http://poser.pugx.org/Reposmc/loginsv/require/php)](https://packagist.org/packages/Reposmc/loginsv)

### Instalación

Instalar el paquete usando el siguiente comando,

    composer require Reposmc/loginsv
    
## Registrar el proveedor de servicios

Agregar el proveedor de servicios en `config/app.php` dentro de la sección de `providers`.
    
    Reposmc\LoginSv\LoginSvServiceProvider::class

## Publicar los recursos

Correr el siguiente comando para la publicación del archivo de configuración en `config/loginsv.php` y el controlador
en `App\Http\Controller\LoginSvController.php`.

    php artisan vendor:publish --provider="Reposmc\Loginsv\LoginSvServiceProvider"
    
## Registrar las credenciales

Agregar las credenciales dentro del `.env`.

    LOGIN_SV_CLIENT_ID='client_id'
    LOGIN_SV_CLIENT_SECRET='client_secret'
    LOGIN_SV_REDIRECT='https://project_url/callback'
    
## Registrar credenciales en Services.php

Agregar las referencias del controlador dentro del archivo de configuración `config/services.php`

    'loginsv' => [
        'client_id' => env('LOGIN_SV_CLIENT_ID'),
        'client_secret' => env('LOGIN_SV_CLIENT_SECRET'),
        'redirect' => env('LOGIN_SV_REDIRECT'),
    ],
    
## Rutas disponibles

Esta ruta redirige a la ruta del proveedor para ser autenticado.
    
    /redirectToProvider
    
Esta ruta es la que retorna el proveedor luego de ser autenticado.

    /callback

