<?php

namespace Reposmc\Backup\Facades;

use \Illuminate\Support\Facades\Facade;

class LoginSvAuth extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'loginsvcontroller';
    }
}
